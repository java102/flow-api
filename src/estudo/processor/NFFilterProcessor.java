package estudo.processor;

import estudo.model.NotaFiscal;

import java.util.concurrent.SubmissionPublisher;

import static java.util.concurrent.Flow.Processor;
import static java.util.concurrent.Flow.Subscription;

// filtra notas com valor maior que zero
public class NFFilterProcessor extends SubmissionPublisher<NotaFiscal> implements Processor<NotaFiscal, NotaFiscal> {

    private Subscription subscription;

    @Override
    public void onSubscribe(final Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(NotaFiscal nf) {
        // filtra notas com valor maior que zero
        if (nf.getValor() > 0.0) {
            submit(nf);
        } else {
            System.out.println("Notas com valor menor ou igual a zero não serão enviadas");
        }
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onComplete() {
        close();
    }

}
