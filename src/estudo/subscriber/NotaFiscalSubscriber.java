package estudo.subscriber;

import estudo.model.NotaFiscal;
import estudo.wsclient.NotaFiscalWSClient;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

// é registrada no SubmissionPublisher da classe Teste
public class NotaFiscalSubscriber implements Subscriber<NotaFiscal> {

    private Subscription subscription;

    @Override // chamado somente uma vez
    public void onSubscribe(final Subscription subscription) {
        System.out.println("Chamando o onSubscribe!");
        this.subscription = subscription;
        this.subscription.request(1); // processa de um em um
    }

    @Override // quando entrar no processamento
    public void onNext(final NotaFiscal notaFiscal) {
        final NotaFiscalWSClient nfwsc = new NotaFiscalWSClient();
        nfwsc.enviar(notaFiscal);
        this.subscription.request(1); // solicita mais um para processar
    }

    @Override
    public void onError(final Throwable throwable) {
        System.err.println("De ruim");
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Todas as notas foram emitidas!");
    }

}
