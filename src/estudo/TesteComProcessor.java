package estudo;

import estudo.model.NotaFiscal;
import estudo.processor.NFFilterProcessor;
import estudo.subscriber.NotaFiscalSubscriber;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.SubmissionPublisher;

public class TesteComProcessor {

    public static void main(String... args) {
        final SubmissionPublisher<NotaFiscal> publisher = new SubmissionPublisher<>();
        final NotaFiscalSubscriber subscriber = new NotaFiscalSubscriber();
        final NFFilterProcessor filter = new NFFilterProcessor();

        // deve-se respeitar essa ordem
        publisher.subscribe(filter);
        filter.subscribe(subscriber);


        getNotas().forEach(publisher::submit);

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        scanner.close();

        publisher.close();
    }

    public static List<NotaFiscal> getNotas() {
        final Random random = new Random();
        return List.of(
                new NotaFiscal("NF 1", random.nextDouble(), LocalDate.now()),
                new NotaFiscal("NF 2", 0.0, LocalDate.now()),
                new NotaFiscal("NF 3", 0.0, LocalDate.now()),
                new NotaFiscal("NF 4", random.nextDouble(), LocalDate.now())
        );
    }

}
